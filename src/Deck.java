import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Locale.filter;

public class Deck {
    public ArrayList<Card> deck = new ArrayList<>();

    //Task a
    public void createDeck() {
        for (int i = 1; i < 14; i++) {
            deck.add(new Card('H', i));
            deck.add(new Card('D', i));
            deck.add(new Card('S', i));
            deck.add(new Card('C', i));
        }

    }

    //Task b
    private ArrayList assign(int n) {
        ArrayList<Card> randomList = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            int randomIndex = rand.nextInt(deck.size());
            Card randomElement = deck.get(randomIndex);
            randomList.add(randomElement);
        }
        return randomList;
    }

    //Task c
    public void getSpades() {
        deck
                .stream()
                .filter(deck -> (deck.getSuit() == 'S'))
                .forEach(card -> System.out.println(card.getDetails()));
    }

    //Task d
    public List<Card> getHeart() {
        return deck
                .stream()
                .filter(deck -> (deck.getSuit() == 'H'))
                .collect(Collectors.toList());
    }

    //Task e
    public List<Character> getColour() {
        return deck
                .stream()
                .map(card -> card.getSuit())
                .collect(Collectors.toList());
    }

    //Task f
    public int sumOfCards() {
       return deck
                .stream()
                .map(card -> card.getFace())
                .reduce(0,(total, sum) -> total + sum);
    }

    //Task g
    public boolean QueenOfSpades() {
        Predicate<Card> p1 = c -> c.getFace() == 12 && c.getSuit() == 'S';
        return deck
                .stream()
                .allMatch(p1);
    }

    //Task h

    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.createDeck();
        //deck.assign(2);
        //System.out.println(deck.getHeart());
        //deck.getSpades();
        deck.getHeart();
        deck.QueenOfSpades();
    }
}
